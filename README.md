Asset Pack
==========

[Asset Pack](https://gitlab.com/bztsrc/assetpack) is a dependency-free single header ANSI C/C++ library that single handedly
solves *ALL* asset related problems in programs (mostly games). It is a specialized form of archiver, comes with the library
that you integrate into your program which handles the asset pack and decodes the assets for you, and a command line utility
to *pack* and *unpack* these archives. It does not support arbitrary directories (use zip for that), rather just specific
directories depending on asset type, and it also creates sprite atlases automatically and has a sophisticated DLC support
(that is, you can patch the base archive with further smaller archives in run-time), and it can encrypt your asset pack (the
tool has a reference AES hook, but you can use any cipher you want).

Furthermore the archive has strict policies on asset formats, only those supported that have a single header library to be
decoded. This keeps the embedded library in your program very small and efficient. For sounds and music OGG, for images PNG,
for fonts SSFN2 (capable to store vector, bitmap and even colorful pixel fonts), for models Model3D (a very featureful format
that can store skeletal animations and voxel worlds too).

For video no single header library exists, but to avoid patent-trolling and licensing issues, OGV Theora was choosen as the
only allowed format. Decoding video therefore requires the libtheoradec library.

Usage
-----

Just download, no dependencies, no install, portable executables.

- [apck-linux.zip](https://gitlab.com/bztsrc/assetpack/raw/main/apck-linux.zip) (Linux, 1M)
- [apck-windows.zip](https://gitlab.com/bztsrc/assetpack/raw/main/apck-windows.zip) (Windows, 1M)

You use the [apck](src) tool to create archive files (or you can integrate [apck_pack.h](apck_pack.h) in your own tool or
editor). This command line utility does the heavy lifting of converting various formats into the ones allowed in Asset Packs.

Then you include [apck.h](apck.h) in your C/C++ source, and in exactly one of your source files, you also define
`APCK_IMPLEMENTATION`. No further libraries and linking needed, it just works (except for theora).

Quick example:

```c
#define APCK_IMPLEMENTATION
#include <apck.h>

apck_t ctx;

/* initialize context with your game's magic bytes and maximum supported engine version */
apck_init(&ctx, "MyLittleGame", 0x1020300, NULL, NULL);

/* first you add the base game to the context */
apck_load(&ctx, "mygame.apck");
/* then you can add any further optional DLCs */
apck_load(&ctx, "moremaps_dlc.apck");
apck_load(&ctx, "ufoarc_dlc.apck");
apck_load(&ctx, "german_lang.apck");

/* set the locale, you can call this any time */
apck_setlocale(&ctx, "en_US");

/* get translated strings */
localized_string = apck_msgstr(&ctx, "Hello World!");
/* get the translated string rendered as a pixel buffer */
rgba_pixelbuffer = apck_text(&ctx, "Hello World!", 0xFF7F7F7F, APCK_BOLD, 24, &width, &height);

/* get unencrypted, uncompressed and decoded (and potentially localized) assets */
rgba_pixelbuffer = apck_image(&ctx, APCK_IMAGE, "texture1", &width, &height);
raw_pcm_data = apck_audio(&ctx, APCK_BGM, "beach", &numsamples, &channels);

/* get any arbitrary unencrypted, uncompressed asset from the archive */
script = apck_asset(&ctx, APCK_SCRIPT, "initialize", &size);
config = apck_asset(&ctx, APCK_OBJECT, "ui_config", &size);

/* free internal buffers and release context */
apck_free(&ctx);
```

Documentation
-------------

- using the [apck](docs/apck.md) utility
- [APCK API](docs/API.md)
- [APCK File Format Specification](docs/apck_spec.md)

Used Resources
--------------

- OGG decoder: [stb_vorbis](https://github.com/nothings/stb/blob/master/stb_vorbis.c)
- PNG decoder: [stb_image](https://github.com/nothings/stb/blob/master/stb_image.h)
- SSFN2 decoder: [SSFN2](https://gitlab.com/bztsrc/scalable-font2)
- 3D decoder: [Model3D](https://bztsrc.gitlab.io/model3d/)
- OGV decoder: [Xiph.org Theora](https://github.com/xiph/theora)

License
-------

Licensed under the terms of the permissive **MIT license**, see [LICENSE](LICENSE).

Cheers,
bzt
