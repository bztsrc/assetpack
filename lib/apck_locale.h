/*
 * lib/apck_locale.h
 * https://gitlab.com/bztsrc/assetpack
 *
 * Copyright (C) 2024 bzt, MIT license
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL ANY
 * DEVELOPER OR DISTRIBUTOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @brief Localized string functions
 */

#ifndef APCK_LOCALE_H
#define APCK_LOCALE_H

int apck_setlocale(apck_t *ctx, char *locale);
char *apck_msgstr(apck_t *ctx, char *msgid);

#ifdef APCK_IMPLEMENTATION

/**
 * Helper to sort msgids
 */
static char *_apck_sort_msgid = NULL;
int _apck_msgidcmp(const void *a, const void *b)
{
    if(!_apck_sort_msgid) return 0;
    return strcmp(_apck_sort_msgid + *((uint32_t*)a), _apck_sort_msgid + *((uint32_t*)b));
}

/**
 * Merge localized strings
 */
static int _apck_merge_locale(apck_t *ctx, uint32_t locale, char *str, uint32_t size)
{
    char *msgid, *msgstr, *ptr, *end;
    int i;

    /* concatenate locale buffers */
    if(!(ctx->files[locale][0].buf = (uint8_t*)realloc(ctx->files[locale][0].buf, ctx->files[locale][0].size + size))) {
        DBG(("unable to allocate memory for locale %u strings\n", locale));
        return 0;
    }
    _apck_sort_msgid = (char*)ctx->files[locale][0].buf;
    ptr = (char*)ctx->files[locale][0].buf + ctx->files[locale][0].size;
    end = ptr + size;
    memcpy(ptr, str, size);
    ctx->files[locale][0].size += size;

    /* merge msgids */
    while(ptr < end) {
        msgid = ptr; ptr += strlen(ptr) + 1;
        msgstr = ptr; ptr += strlen(ptr) + 1;
        /* look if we already have this msgid */
        for(i = 0; i < ctx->files[locale][0].w && strcmp(_apck_sort_msgid + ctx->msgstr[locale][i * 2], msgid); i++);
        if(i >= ctx->files[locale][0].w) {
            /* no, add new index */
            i = ctx->files[locale][0].w++;
            if(!(ctx->msgstr[locale] = (uint32_t*)realloc(ctx->msgstr[locale], ctx->files[locale][0].w * 2 * sizeof(uint32_t)))) {
                DBG(("unable to allocate memory for locale %u index\n", locale));
                ctx->files[locale][0].w = 0;
                return 0;
            }
            ctx->msgstr[locale][i * 2] = msgid - _apck_sort_msgid;
        }
        /* update the msgstr pointer in index */
        ctx->msgstr[locale][i * 2 + 1] = msgstr - _apck_sort_msgid;
    }

    /* sort locale index so that we can do an O(log2) search later */
    if(ctx->files[locale][0].w && ctx->msgstr[locale])
        qsort(ctx->msgstr[locale], ctx->files[locale][0].w, 2 * sizeof(uint32_t), _apck_msgidcmp);
    return 1;
}

/**
 * Select a locale
 */
int apck_setlocale(apck_t *ctx, char *locale)
{
    uint32_t i;

    if(!ctx || !locale || !*locale) return APCK_ERR_BADINP;

    /* first, try the entire locale name, like "en_US" */
    for(i = 0; i < APCK_FONT && ctx->files[i] && strcmp(ctx->files[i][0].name, locale); i++);
    if(i >= APCK_FONT) {
        /* if not found, then try the ISO-639-1 part only, like "en" */
        for(i = 0; i < APCK_FONT && ctx->files[i] && memcmp(ctx->files[i][0].name, locale, 2); i++);
        if(i >= APCK_FONT) return APCK_ERR_BADINP;
    }
    /* check if it has a locale index, should have, but be paranoid */
    if(!ctx->msgstr[i]) return APCK_ERR_BADINP;
    ctx->locale = i;
    return APCK_OK;
}

/**
 * Look up a localized string
 */
char *apck_msgstr(apck_t *ctx, char *msgid)
{
    uint32_t *idx;
    char *str;
    int s = 0, e, h, m;

    if(!ctx || !msgid || !*msgid || ctx->locale >= APCK_FONT || !ctx->msgstr[ctx->locale] ||
      !ctx->files[ctx->locale] || !ctx->files[ctx->locale][0].w || !ctx->files[ctx->locale][0].buf)
        return msgid;

    /* use binary search */
    str = (char*)ctx->files[ctx->locale][0].buf;
    idx = ctx->msgstr[ctx->locale];
    e = ctx->files[ctx->locale][0].w - 1;
    while(s <= e) {
        h = ((e + s) >> 1);
        m = strcmp(str + idx[h * 2], msgid);
        if(!m) return str + idx[h * 2 + 1];
        if(m > 0) e = h - 1; else s = h + 1;
    }
    return msgid;
}

#endif /* APCK_IMPLEMENTATION */

#endif /* APIC_LOCALE_H */
