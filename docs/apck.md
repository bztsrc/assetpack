Asset Pack Utility
==================

Usage
-----

```
 apck [-k <key>] [-e <engine>] [-v <version>] [-q] <input directory> <output.apck>
 apck [-k <key>] <input.apck> <output directory>
 apck [-k <key>] -d <input.apck>

  -k <key>      specify AES-CBC-256 encryption key (32 hex digits)
  -e <engine>   specify your engine's magic, up to 12 ASCII letters
  -v <version>  specify your engine's version, in X.X.X format
  -d            dump asset directory in archive
  -h            help, required directory structure
  -q            quiet, surpress conversion errors
```

If the first argument is a directory, then it creates an archive. Otherwise if the first argument is an archive, then it unpacks.
The directory might not contain just any files, there are rules and its directory structure is strict (but only the top level,
you are free to use subfolders to organize your assets any way you seem fit).

The library allows any kind of encryption, the `apck` tool comes with a reference AES implementation. For this cipher, a 16
bytes long `key` should be embedded in your engine along with two hooks, and you set that key to the tool with the `-k` flag.

Directory Structure
-------------------

### icon.png

If this file exists, then it is added to images and used as the application icon.

### locales/

Files under this subfolder must be named as a two letter ISO-639-1 language code optionally followed by an underscore and a
two bytes region code with the extension `.po`. There could be subdirectories with matching names, for example `locales/en_US.po`
and `locales/en_US/`.

If such subfolders exists, then files in those can be either sound files, which will be added as localized voices, or images
with text on them, which will be added as images. Supported file formats: WAV, MP3 and OGG; PNG, JPEG, WEBP, GIF, BMP, TGA. Both
audio and image files are automatically converted.

### images/

Files in this subfolder will be added as images without parsing and they must not contain translateable text on them. Supported
file formats: PNG, JPEG, WEBP, GIF, BMP, TGA. Image files are automatically converted. (For images with texts, see `locales/`.)

### sprites/

Files in this subfolder will be parsed, packed in sprite atlases and added as PNG texture images as well as sprite animations.
Supported file formats: PNG, JPEG, WEBP, GIF, BMP, TGA.

The images should contain multiple animation frames, which must be separated by an empty, fully transparent pixel (both
horizontally and vertically). Frames are taken left to right, top to bottom. The file name might be suffixed with a combination
of playback type, `_loop` (play in a loop, default), `_once` (play once), `_bf` (back and forth); and a direction `_s` (South,
default), `_sw`, `_w`, `_nw`, `_n`, `_ne`, `_e`, `_se`. For example `hero_walking_nw_bf.webp` will be parsed for animation frames,
and will be saved as asset `hero_walking`, played as a back and forth animation where the hero walks in the North-West direction.

If the file name is suffixed as `_wang` or `_wall`, then those are autotiles. This means they follow a special, engine-specific
arrangement, so Asset Pack does not parse these for sprites, it just stores them as-is.

### bgm/

Files in this subfolder will be added as background music. Supported file formats: WAV, MP3 and OGG. Audio files are automatically
converted.

### sfx/

Files in this subfolder will be added as sound effects and must not contain speech. Supported file formats: WAV, MP3 and OGG.
Audio files are automatically converted. (For audio with speech, see `locales/`.)

### fonts/

Files in this subfolder will be added as fonts. Supported file formats: TTF, OTF, PFA, PFB, WOFF, WOFF2, SFD, BDF, PCF, PSF,
PSFU, FON, FNT, KBITS, KBITX, BMF, YAFF and SSFN2. Font files are automatically converted.

### videos/

Files under this subfolder must be in [OGG Theora](https://www.theora.org/) format, and they will be added as video assets.
[ffmpeg](https://ffmpeg.org/) can be used to convert video files from almost any existing video format into OGV.

### models/

Files under this subfolder must be in [Model3D](https://bztsrc.gitlab.io/model3d/) format, and they will be added as 3D models.
[Blender plugin](https://www.blender.org/) can export in this format, and [m3dconv](https://gitlab.com/bztsrc/model3d/-/tree/master/m3dconv)
can be used to convert from almost any existing model format (including skeletal animations and voxel images; like FBX, DAE, GLTF,
3DS, PLY, VOX, BONVOX, Minecraft SCHEM, Wavefront OBJ, Milkshape3D, Miku Miku Dance PMX, etc.).

### maps/

Files under this subfolder must be in [Tiled TMX](https://doc.mapeditor.org/en/stable/reference/tmx-map-format/) format. They
can be edited with [Tiled Mapeditor](https://www.mapeditor.org/) or with a plain simple text editor (it is an XML based format).

### objects/

Files under this subfolder can be in any format, and they will be added to the archive verbatim.

### scripts/

Files under this subfolder can be in any format, and they will be added to the archive verbatim.

### data01/ ... data16/

Files under these subfolders can be in any format, and they will be added to the archive verbatim. There are 16 of these
directories to help you organize your further files whatever type they might be. These assets can be accessed separately by
their category in the APCK API.
