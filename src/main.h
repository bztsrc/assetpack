/*
 * src/main.h
 * https://gitlab.com/bztsrc/assetpack
 *
 * Copyright (C) 2024 bzt, MIT license
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL ANY
 * DEVELOPER OR DISTRIBUTOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @brief Main header file for the apck utility
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <apck.h>

extern int quiet;

/* file.c */
typedef int (*filecb_t)(char *fn);
extern int pathlen;
int base64_decode(char *s, uint8_t *out, int len);
void apck_mkdir(char *fn);
int isfile(char *fn);
uint8_t* getfile(char *fn, uint64_t *size);
int putfile(char *fn, uint8_t *buf, uint64_t size);
int find(char *path, int subdirs, filecb_t filecb);

/* aes.c */
extern uint8_t aes_ivkey[16];

/* pack.c */
int pack(char *input, void *f, char *engine, uint32_t version, int encrypt, int maxsize);

/* unpack.c */
int unpack(char *input, char *output);

/* audio.c */
uint8_t *audio_decode(uint8_t *buf, uint64_t size, int ismusic, uint64_t *len);

/* image.c */
uint32_t *image_decode(uint8_t *buf, uint64_t size, int *w, int *h);

/* font.c */
uint8_t *font_decode(char *filename, uint8_t *buf, uint64_t size, uint32_t *len);

/* po.c */
uint8_t *po_decode(uint8_t *buf, uint64_t size, uint64_t *len);
uint8_t *po_encode(uint8_t *buf, uint64_t len, uint64_t *size);

/* tmx.c */
apck_map_t *tmx_decode(uint8_t *buf, uint64_t size);
uint8_t *tmx_encode(apck_map_t *map, uint64_t *size);
