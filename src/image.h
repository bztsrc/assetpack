/*
 * src/image.h
 * https://gitlab.com/bztsrc/assetpack
 *
 * Copyright (C) 2024 bzt, MIT license
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL ANY
 * DEVELOPER OR DISTRIBUTOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * @brief Image header, mostly sprite atlas packer stuff
 */

typedef struct {
    int a, x, y, w, h;
} atlas_slot_t;

typedef struct {
    int idx, dir, frm;
    int a, x, y, w, h, l, t, p, o;
    uint32_t *data;
} atlas_spr_t;

typedef struct {
    uint64_t sp;
    int mw, mh, numimg, size;
    uint32_t **imgs;
    int numslot;
    atlas_slot_t *slots;
    int numspr;
    atlas_spr_t *spr;
    int len, nd;
} atlas_t;
extern atlas_t atlas;

typedef struct {
    int w, h;
    uint32_t *pix;
} image_buf_t;

typedef struct {
    char name[FILENAME_MAX + 1];
    image_buf_t buf[APCK_NUMDIR];
    apck_sprite_t sprite;
} image_t;
extern image_t *images;
extern int numimages;
extern char *dirext[], *loopext[];

int image_add(char *name, uint32_t *pix, int w, int h);
int image_build_atlas(int maxsize, int doop);
